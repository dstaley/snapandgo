package snapandgo

import (
	"appengine"
	"appengine/urlfetch"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

type SnapchatUser struct {
	Id          string   `json:"id,omitempty"`
	BestFriends []string `json:"best_friends,omitempty"`
}

var re = regexp.MustCompile("<div class=\"best_name\"><a href=\"/.*?\">(.*?)</a></div>")

func handler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	client := urlfetch.Client(c)
	res, err := client.Get("http://www.snapchat.com/" + r.URL.Path[1:])
	if err != nil {
		log.Fatal(err)
	}
	robots, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}

	var bestFriends []string
	for _, element := range re.FindAllStringSubmatch(string(robots), -1) {
		bestFriends = append(bestFriends, element[1])
	}
	user := SnapchatUser{
		Id:          r.URL.Path[1:],
		BestFriends: bestFriends,
	}
	b, err := json.Marshal(user)
	if err != nil {
		fmt.Println("error:", err)
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "%s\n", b)
}

func init() {
	http.HandleFunc("/", handler)
}
