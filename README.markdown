# SnapandGo
SnapandGo is a simple App Engine app that fetches the Snapchat profile for a given user and returns a JSON response containing their best friends. Or at least it did until Snapchat disabled the user profile pages. It's not like I spent a large amount of time using these profile pages to generate force directed graphs of Snapchat networks or anything.

![](http://replygif.net/i/258.gif)

For an example of some of the awesome visualizations I was able to make (WHILE THE FUN LASTED), [here's one with about 73k nodes](http://dstaleyscreenshots.s3.amazonaws.com/SnapchatNetwork.pdf). (8MB PDF)